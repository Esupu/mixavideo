package eshael.com.mixaplayer.util;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import eshael.com.mixaplayer.R;
import eshael.com.mixaplayer.adapter.ViewPagerAdapter;

/**
 * Created by Raphael on 21-06-2015.
 */
public class TabControlFragment extends Fragment
{
	Toolbar _toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"NEW RELEASE","CATEGORY"};
    int Numboftabs =2;

    public TabControlFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
    {
        // Inflate the layout for this fragment
    	 // Creating The Toolbar and setting it as the Toolbar for the activity
    	 
    	View rootView = inflater.inflate(R.layout.sliding_tab, container,false);
    	
       // _toolbar = (Toolbar) rootView.findViewById(R.id.tool_bar);
        //setSupportActionBar(_toolbar);
 
        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerAdapter(this.getFragmentManager(),Titles,Numboftabs);
 
        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) rootView.findViewById(R.id.pager);
        pager.setAdapter(adapter);
 
        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) rootView.findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
 
        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }

        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    	
        return rootView;//inflater.inflate(R.layout.fragment_health, container, false);
    }

}
