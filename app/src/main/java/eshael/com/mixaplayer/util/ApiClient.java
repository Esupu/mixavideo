package eshael.com.mixaplayer.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Raphael on 7/11/2016.
 */
public class ApiClient {

        public static final String BASE_URL = "http://video-mspeeda.rhcloud.com/video/";
//        public static final String BASE_URL = "http://192.168.137.199:8080/video/";
        private static Retrofit retrofit = null;


        public static Retrofit getClient() {
            if (retrofit==null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
}
