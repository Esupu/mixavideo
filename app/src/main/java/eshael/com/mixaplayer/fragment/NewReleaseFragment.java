package eshael.com.mixaplayer.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.orm.SugarContext;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

import eshael.com.mixaplayer.MainActivity;
import eshael.com.mixaplayer.R;
import eshael.com.mixaplayer.VideoDetailActivity;
import eshael.com.mixaplayer.adapter.CategoryAdapter;
import eshael.com.mixaplayer.adapter.VideoAdapter;
import eshael.com.mixaplayer.model.Category;
import eshael.com.mixaplayer.model.Video;
import eshael.com.mixaplayer.util.ApiClient;
import eshael.com.mixaplayer.util.ApiInterface;
import eshael.com.mixaplayer.util.ItemClickSupport;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Raphael on 7/7/2016.
 */
public class NewReleaseFragment extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = NewReleaseFragment.class.getSimpleName();
    private View rootView;
    private List<Video> videoList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        SugarContext.init(getActivity());
        rootView = inflater.inflate(R.layout.fragment_new_release,container,false);
        swipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.refresh_latest);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.videos_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ItemClickSupport.addTo(recyclerView)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v)
                    {
                        Log.d("Position",position+"");
                        Log.d(TAG, "Number of movies received: " + videoList.get(position).getTitle());

                        Intent intent = new Intent(getActivity(), VideoDetailActivity.class);
                            intent.putExtra("title",videoList.get(position).getTitle());
                            intent.putExtra("category",videoList.get(position).getCategory());
                            intent.putExtra("duration",videoList.get(position).getDuration());
                            intent.putExtra("video_url",videoList.get(position).getVideo_url());
                            intent.putExtra("thumbnail",videoList.get(position).getThumbnail());
                            intent.putExtra("desc",videoList.get(position).getDescription());

                        startActivity(intent);
                    }
                });
        loadContent();
        return rootView;
    }

    private void loadContent()
    {
        List<Video> db = Video.listAll(Video.class);
        if(db.size()>1)
        {
            videoList = db;
            recyclerView.setAdapter(new VideoAdapter(videoList, R.layout.video_row, getContext()));
        }else
        {
            if (haveNetworkConnection())
            {
                getVideos();
            }
            else
            {
                swipeRefreshLayout.setRefreshing(true);
//                Snackbar.make(rootView, "No internet connection !", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void getVideos()
    {
        ApiInterface apiService =  ApiClient.getClient().create(ApiInterface.class);

        Call<List<Video>> call = apiService.getVideos("videos");
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response)
            {
                videoList = response.body();
                Log.d(TAG, "Number of movies received: " + videoList);
                for (Video video : videoList)
                {
                    video.save();
                }
                List<Video>  videoList= Video.listAll(Video.class);
                if (videoList.size()>1)
                    recyclerView.setAdapter(new VideoAdapter(videoList, R.layout.video_row, getContext()));
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Video>>call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Log.d("position ",view.getHandler()+"");
    }

    @Override
    public void onRefresh() {
        if (haveNetworkConnection())
        {
            getVideos();
        }
        else
        {
            swipeRefreshLayout.setRefreshing(false);
//            Snackbar.make(rootView, "No internet connection !", Snackbar.LENGTH_LONG).show();
        }
    }


    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
