package eshael.com.mixaplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import eshael.com.mixaplayer.adapter.VideoAdapter;
import eshael.com.mixaplayer.model.Video;
import eshael.com.mixaplayer.util.ApiClient;
import eshael.com.mixaplayer.util.ApiInterface;
import eshael.com.mixaplayer.util.ItemClickSupport;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Raphael on 7/15/2016.
 */
public class CategoryVideoActivity extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = CategoryVideoActivity.class.getSimpleName();
    private List<Video> videoList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_new_release);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh_latest);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(this);
        }
        recyclerView = (RecyclerView) findViewById(R.id.videos_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemClickSupport.addTo(recyclerView)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v)
                    {
                        Log.d("Position",position+"");
                        Log.d(TAG, "Number of movies received: " + videoList.get(position).getTitle());

                        Intent intent = new Intent(CategoryVideoActivity.this, VideoDetailActivity.class);
                        intent.putExtra("title",videoList.get(position).getTitle());
                        intent.putExtra("category",videoList.get(position).getCategory());
                        intent.putExtra("duration",videoList.get(position).getDuration());
                        intent.putExtra("video_url",videoList.get(position).getVideo_url());
                        intent.putExtra("thumbnail",videoList.get(position).getThumbnail());
                        intent.putExtra("desc",videoList.get(position).getDescription());

                        startActivity(intent);
                    }
                });
        getVideos();

    }
    private void getVideos()
    {
        ApiInterface apiService =  ApiClient.getClient().create(ApiInterface.class);

        Call<List<Video>> call = apiService.getCategoryVideos("category",getIntent().getStringExtra("category"));
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response)
            {
                videoList = response.body();
                Log.d(TAG, "Number of movies received: " + videoList);
//                SugarRecord.saveInTx(videoList);
//                List<Video> db = Video.listAll(Video.class);
                if (videoList.size()>1)
                    recyclerView.setAdapter(new VideoAdapter(videoList, R.layout.video_row, CategoryVideoActivity.this));
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Video>>call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public void onRefresh() {
        getVideos();
    }
}
